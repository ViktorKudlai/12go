<?php

namespace Service;

use Exception\InputFileException;
use Exception\OutputFileException;
use Exception\InvalidCaseLineException;

/**
 * Class LocationFinder implements solution to solve the
 * problem described here - https://open.kattis.com/problems/alldifferentdirections
 *
 * @package Service
 */
class LocationFinder
{
    const DEFAULT_ROUND_PRECISION = 4;

    /**
     * Array of all cases results.
     *
     * @var array
     */
    protected $locationResults = [];

    /**
     * Handle data from file.
     *
     * @param string $inputFile
     *
     * @throws InputFileException
     */
    public function handleDataFromFile(string $inputFile)
    {
        $fileHandler = fopen($inputFile, 'r');
        if (!$fileHandler) {
            throw new InputFileException();
        }

        while (false !== ($line = trim(fgets($fileHandler)))) {
            $peopleNumberInCase = (int)$line;
            if (!ctype_digit($line) || 0 == $peopleNumberInCase) {
                break;
            }

            $caseData = [];
            for ($peopleCounter = 0; $peopleCounter < $peopleNumberInCase; $peopleCounter++) {
                $caseData[] = trim(fgets($fileHandler));
            }
            $this->handleCase($caseData);
        }
        fclose($fileHandler);
    }

    /**
     * Write results into file.
     *
     * @param string $outputFilename Filename with relative path.
     *
     * @throws OutputFileException
     */
    public function writeResultToFile(string $outputFilename)
    {
        $fileHandler = fopen($outputFilename, 'w');
        if (!$fileHandler) {
            throw new OutputFileException();
        }

        foreach ($this->locationResults as $caseResult) {
            $outLine = "{$caseResult['averageDistance']['x']} {$caseResult['averageDistance']['y']} {$caseResult['longestDistance']}" . PHP_EOL;
            fputs($fileHandler, $outLine);
        }
        fclose($fileHandler);
    }

    /**
     * Return array of location results.
     *
     * @return array
     */
    public function getResults(): array
    {
        return $this->locationResults;
    }

    /**
     * Handle specified case and calculate
     * average and the longest distance to
     * destination point.
     *
     * @param array $caseData
     */
    protected function handleCase(array $caseData)
    {
        $destinationCoordinates = [];
        foreach ($caseData as $caseLine) {
            $destinationCoordinates[] = $this->handleLine($caseLine);
        }

        $averageDistanceToPoint = $this->calculateAverageDistanceToPoint($destinationCoordinates);
        $longestDistanceToPoint = $this->calculateLongestDistanceToPoint(
            $averageDistanceToPoint,
            $destinationCoordinates
        );

        $this->locationResults[] = [
            'averageDistance' => $averageDistanceToPoint,
            'longestDistance' => $longestDistanceToPoint
        ];
    }

    /**
     * Handle person's direction string.
     *
     * @param string $caseLine
     *
     * @return array Destination point base on person's advice.
     *
     * @throws InvalidCaseLineException
     */
    protected function handleLine(string $caseLine)
    {
        preg_match('/(\-?\d+(?:\.\d{1,4})?)\s(\-?\d+(?:\.\d{1,4})?)\sstart(\s(?:\-?\d+(?:\.\d{1,4})?))/',
            $caseLine,
            $initialData
        );
        if (count($initialData) !== 4) {
            throw new InvalidCaseLineException();
        }
        list(, $currentPoint['x'], $currentPoint['y'], $angleInDegrees) = $initialData;

        preg_match_all('((?:walk|turn)\s(?:\-?\d+(?:\.\d{1,4})?))', $caseLine, $instructions);
        $instructions = array_shift($instructions);

        foreach ($instructions as $instruction) {
            list($action, $argument) = explode(' ', $instruction);
            switch ($action) {
                case 'turn':
                    $angleInDegrees += $argument;
                    break;
                case 'walk':
                    $currentPoint = $this->moveToPoint($currentPoint, $argument, $angleInDegrees);
                    break;
            }
        }
        $destinationPoint = $currentPoint;

        return $destinationPoint;
    }

    /**
     * Moves current point to new by direction parameters.
     *
     * @param array $currentPoint
     * @param float $distance
     * @param float $angleInDegrees
     *
     * @return array New point coordinates.
     */
    protected function moveToPoint(array $currentPoint, float $distance, float $angleInDegrees): array
    {
        $newPoint = [];
        $angleInRadians = $angleInDegrees * M_PI / 180;
        $newPoint['x'] = $currentPoint['x'] + $distance * cos($angleInRadians);
        $newPoint['y'] = $currentPoint['y'] + $distance * sin($angleInRadians);

        return $newPoint;
    }

    /**
     * Calculate average destination to point
     * by given array of coordinates.
     *
     * @param array $destinationCoordinates
     *
     * @return array
     */
    protected function calculateAverageDistanceToPoint(array $destinationCoordinates): array
    {
        $sumOfX = 0;
        $sumOfY = 0;
        $averageDestinationPoint = [];
        $destinationsCount = count($destinationCoordinates);

        foreach ($destinationCoordinates as $pointCoordinates) {
            $sumOfX += $pointCoordinates['x'];
            $sumOfY += $pointCoordinates['y'];
        }

        $averageDestinationPoint['x'] = round(
            $sumOfX / $destinationsCount,
            static::DEFAULT_ROUND_PRECISION
        );
        $averageDestinationPoint['y'] = round(
            $sumOfY / $destinationsCount,
            static::DEFAULT_ROUND_PRECISION
        );

        return $averageDestinationPoint;
    }

    /**
     * Calculate the longest distance to point
     * by average distance to point and array of coordinates.
     *
     * @param $averageDistanceToPoint
     * @param $destinationCoordinates
     *
     * @return float
     */
    protected function calculateLongestDistanceToPoint(
        $averageDistanceToPoint,
        $destinationCoordinates
    ): float
    {
        $longestDistance = 0;
        foreach ($destinationCoordinates as $pointCoordinates) {
            $distance = $this->calculateDistanceBetweenTwoPoints($pointCoordinates, $averageDistanceToPoint);
            if ($distance > $longestDistance) {
                $longestDistance = $distance;
            }
        }

        return $longestDistance;
    }

    /**
     * Calculate the distance between two points.
     *
     * @param array $point1 Array of first point coordinates.
     * @param array $point2 Array of second point coordinates.
     *
     * @return float
     */
    protected function calculateDistanceBetweenTwoPoints(array $point1, array $point2): float
    {
        $distance = sqrt(
            pow($point1['x'] - $point2['x'], 2) +
            pow($point1['y'] - $point2['y'], 2)
        );

        return round($distance, static::DEFAULT_ROUND_PRECISION);
    }
}

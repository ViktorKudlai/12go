#All Different Directions

This service helps to find right direction after taking advice from `n` persons.
More information about the task you can find by [link.](https://open.kattis.com/problems/alldifferentdirections)

##Usage
1. Edit _*sample.in*_ file with directions data if you want to chane default input values.
2. Run script from CLI by executing `php Run.php`.
3. Result will be able in _*sample.out*_ file.
4. Also result outputs into console. 

##Console output example
![alt text](result.png "Result")
<?php

use Service\LocationFinder;

require 'AutoLoader.php';
$loader = new AutoLoader();
$loader->register();

/** @var LocationFinder $locatorFinder */
$locatorFinder = new LocationFinder();
$locatorFinder->handleDataFromFile('./sample.in');
$locatorFinder->writeResultToFile('./sample.out');
echo json_encode($locatorFinder->getResults(), JSON_PRETTY_PRINT);
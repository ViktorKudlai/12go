<?php

namespace Exception;

use Exception;

class OutputFileException extends Exception
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Error while creating/opening file.';
}

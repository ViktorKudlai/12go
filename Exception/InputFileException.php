<?php

namespace Exception;

use Exception;

class InputFileException extends Exception
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Error while opening input file.';
}

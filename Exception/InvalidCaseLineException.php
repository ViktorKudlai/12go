<?php

namespace Exception;

use Exception;

class InvalidCaseLineException extends Exception
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Invalid case line format.';
}
